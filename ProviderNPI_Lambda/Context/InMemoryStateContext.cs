﻿using DevExpress.Xpo;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using ProviderNPI_Lambda.Models;
using System;
using System.Collections.Generic;

namespace ProviderNPI_Lambda.Context
{
    public class InMemoryStateContext : InMemoryDataContext<StateData>
    {

        ProviderNPIDataContext _RCMContext = null;
        public InMemoryStateContext(ProviderNPIDataContext RCMContext, IHttpContextAccessor contextAccessor, IMemoryCache memoryCache) :
            base(RCMContext, contextAccessor, memoryCache)
        {
            _RCMContext = RCMContext;
            CreateKey = "State";
        }
        public string sWhere { get; set; } = "";
        public ICollection<StateData> tblStates => ItemsInternal;
        protected override IEnumerable<StateData> Source => _RCMContext.tblStates;

        protected override int GetKey(StateData item) => item.State_id;

        protected override void SetKey(StateData item, int key) => item.State_id = key;
    }

}
