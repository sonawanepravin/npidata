﻿using DevExpress.Xpo;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using ProviderNPI_Lambda.Models;
using ProviderNPI_Lambda.Pages;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProviderNPI_Lambda.Context
{

    public abstract class InMemoryDataContext<T> {
        IHttpContextAccessor _contextAccessor;
        IMemoryCache _memoryCache;
        public string CreateKey { get; set; }
        public InMemoryDataContext(ProviderNPIDataContext _RCMContext,IHttpContextAccessor contextAccessor, IMemoryCache memoryCache) {
            _contextAccessor = contextAccessor;
            _memoryCache = memoryCache;
            _memoryCache.Remove("NPIData");
        }

        protected ICollection<T> ItemsInternal {
            get {
                var session = _contextAccessor.HttpContext.Session;
                var key = CreateKey;
                return _memoryCache.GetOrCreate(key, entry => {
                    //session.SetInt32("dirty", 1);
                    entry.SlidingExpiration = TimeSpan.FromHours(10);
                    entry.RegisterPostEvictionCallback(MyCallback);
                    return Source.ToList();
                });
                
            }
        }
        private void MyCallback(object key, object value, EvictionReason reason, object state)
        {
            var message = $"Cache entry was removed : {reason}";
            MemoryCacheLog memoryCache = new MemoryCacheLog() { 
                Key=key,
                Value=value,
                Reason=reason,
                State=state
            };
            //((HomeModel)state).cache.Set("callbackMessage", Newtonsoft.Json.JsonConvert.SerializeObject(memoryCache));
        }
        public class MemoryCacheLog
        {
            public object Key { get; set; }
            public object Value { get; set; }
            public EvictionReason Reason { get; set; }
            public object State { get; set; }
        }
        protected abstract IEnumerable<T> Source { get; }

        public void SaveChanges() {
            foreach(var item in ItemsInternal.Where(i => GetKey(i) == 0))
                SetKey(item, ItemsInternal.Max(GetKey) + 1);
        }

        protected abstract int GetKey(T item);

        protected abstract void SetKey(T item, int key);
    }
}
