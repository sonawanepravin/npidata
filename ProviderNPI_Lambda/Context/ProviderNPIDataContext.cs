﻿using Microsoft.EntityFrameworkCore;
using ProviderNPI_Lambda.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProviderNPI_Lambda.Context
{
    public class ProviderNPIDataContext : DbContext
    {
        public ProviderNPIDataContext(DbContextOptions options)
            : base(options)
        {
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder); 
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder); 
        }
        public DbSet<payments> tblPayments { get; set; }
        public DbSet<CityData> tblCities { get; set; }
        public DbSet<StateData> tblStates { get; set; }
        public DbSet<TaxonomyData> tblTaxonomy { get; set; }
        public DbSet<NPIData> tblNPIData { get; set; }
        public DbSet<vw_npidata_search> tblVW_NPIData_Search { get; set; }
    }
    
}
