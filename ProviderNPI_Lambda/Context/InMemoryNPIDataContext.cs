﻿using DevExpress.Xpo;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using ProviderNPI_Lambda.Models;
using System;
using System.Collections.Generic;

namespace ProviderNPI_Lambda.Context
{
    public class InMemoryNPIContext : InMemoryDataContext<NPIData>
    {

        ProviderNPIDataContext _RCMContext = null;
        public InMemoryNPIContext(ProviderNPIDataContext RCMContext, IHttpContextAccessor contextAccessor, IMemoryCache memoryCache) :
            base(RCMContext, contextAccessor, memoryCache)
        {
            _RCMContext = RCMContext;
            CreateKey = "NPIData";
        }

        public ICollection<NPIData> tblNPIData => ItemsInternal;
        public string sWhere { get; set; } = " where 1=1;";
        protected override IEnumerable<NPIData> Source => _RCMContext.tblNPIData.FromSqlRaw<NPIData>(string.Format("select NPI,Entity_Type_Code,Replacement_NPI,Employer_Identification_Number_EIN,Provider_Organization_Name_Legal_Business_Name,Provider_Last_Name_Legal_Name,Provider_First_Name,Provider_Middle_Name, Provider_Name_Prefix_Text, Provider_Name_Suffix_Text, Provider_Credential_Text, Provider_Other_Organization_Name, Provider_Other_Organization_Name_Type_Code,Provider_Other_Last_Name, Provider_Other_First_Name, Provider_Other_Middle_Name, Provider_Other_Name_Prefix_Text, Provider_Other_Name_Suffix_Text, Provider_Other_Credential_Text,Provider_Other_Last_Name_Type_Code, Provider_First_Line_Business_Mailing_Address, Provider_Second_Line_Business_Mailing_Address, Provider_Business_Mailing_Address_City_Name,Provider_Business_Mailing_Address_State_Name, Provider_Business_Mailing_Address_Postal_Code, Provider_Business_Mailing_Address_Country_Code_out_US,Provider_Business_Mailing_Address_Telephone_Number, Provider_Business_Mailing_Address_Fax_Number, Provider_First_Line_Business_Practice_Location_Address,Provider_Second_Line_Business_Practice_Location_Address, Provider_Business_Practice_Location_Address_City_Name, Provider_Business_Practice_Location_Address_State_Name,Provider_Business_Practice_Location_Address_Postal_Code, Provider_Business_Practice_Location_Address_Country_Code_out_US, Provider_Business_Practice_Location_Address_Telephone_Number,Provider_Business_Practice_Location_Address_Fax_Number, Provider_Enumeration_Date, Last_Update_Date, NPI_Deactivation_Reason_Code, NPI_Deactivation_Date, NPI_Reactivation_Date,Provider_Gender_Code, Authorized_Official_Last_Name, Authorized_Official_First_Name, Authorized_Official_Middle_Name, Authorized_Official_Title_or_Position,Authorized_Official_Telephone_Number, Healthcare_Provider_Taxonomy_Code_1, Provider_License_Number_1, Provider_License_Number_State_Code_1, Healthcare_Provider_Primary_Taxonomy_Switch_1,Is_Sole_Proprietor, Is_Organization_Subpart, Parent_Organization_LBN, Parent_Organization_TIN, Authorized_Official_Name_Prefix_Text,Authorized_Official_Name_Suffix_Text,Authorized_Official_Credential_Text, Healthcare_Provider_Taxonomy_Group_1 from npidata_search {0}",sWhere));


        protected override int GetKey(NPIData item) => item.NPI;

        protected override void SetKey(NPIData item, int key) => item.NPI = key;
    }

}
