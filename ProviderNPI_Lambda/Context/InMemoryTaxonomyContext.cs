﻿using DevExpress.Xpo;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using ProviderNPI_Lambda.Models;
using System.Linq;
using System.Collections.Generic;

namespace ProviderNPI_Lambda.Context
{
    public class InMemoryTaxonomyContext : InMemoryDataContext<TaxonomyData>
    {
        ProviderNPIDataContext _RCMContext = null;
        public InMemoryTaxonomyContext(ProviderNPIDataContext RCMContext, IHttpContextAccessor contextAccessor, IMemoryCache memoryCache) :
            base(RCMContext, contextAccessor, memoryCache)
        {
            _RCMContext = RCMContext;
            CreateKey = "Taxonomy";
        }
        public ICollection<TaxonomyData> tblTaxonomies => ItemsInternal;
        public string sWhere { get; set; } = "";
        protected override IEnumerable<TaxonomyData> Source => _RCMContext.tblTaxonomy;
        protected override int GetKey(TaxonomyData item) => item.ID;
        protected override void SetKey(TaxonomyData item, int key) => item.ID = key;
    }

}
