﻿using DevExpress.Xpo;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using ProviderNPI_Lambda.Models;
using System;
using System.Collections.Generic;
using System.Linq;
namespace ProviderNPI_Lambda.Context
{
    public class InMemoryCityContext : InMemoryDataContext<CityData>
    {

        ProviderNPIDataContext _RCMContext = null;
        public InMemoryCityContext(ProviderNPIDataContext RCMContext, IHttpContextAccessor contextAccessor, IMemoryCache memoryCache) :
            base(RCMContext, contextAccessor, memoryCache)
        {
            _RCMContext = RCMContext;
            CreateKey = "Cities";
        }
        public ICollection<CityData> tblCities => ItemsInternal;
        public string sWhere { get; set; } = "";
        protected override IEnumerable<CityData> Source => _RCMContext.tblCities;
        protected override int GetKey(CityData item) => item.City_Id;
        protected override void SetKey(CityData item, int key) => item.City_Id = key;
    }

}
