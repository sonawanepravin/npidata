using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PRM.SessionHandler;
using ProviderNPI_Lambda.Models;

namespace ProviderNPI_Lambda.Pages
{
    public class ContainerModel : PageModel
    {
        
        public IActionResult OnGet()
        {
            
            return Page();
        }
        public void OnGetDashboard()
        {
        }
        public void OnGetPractice()
        {
        }
        public void OnGetPracticeAssignedToMe()
        {
        }
        public void OnGetProvider()
        {
        }
        public void OnGetFollowUp()
        {
        }
        public void OnGetChangePassword()
        {
        }
        public void OnGetLogout()
        {
        }
    }
}
