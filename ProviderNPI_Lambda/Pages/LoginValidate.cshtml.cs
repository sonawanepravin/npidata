using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PRM.SessionHandler;
using ProviderNPI_Lambda.DAL;
using ProviderNPI_Lambda.Models;

namespace ProviderNPI_Lambda.Pages
{
    public class LoginValidateModel : PageModel
    {
        ProviderNPIDAL provider = null;
        public LoginValidateModel()
        {
            provider = new ProviderNPIDAL();
        }
        public IActionResult OnGet()
        {
            HttpContext.Session.Clear();
            HttpContext.SignOutAsync();
            SessionManager session = new SessionManager(HttpContext.Session);
            session.Set<SignInResult>(SessionKeys.user, null);
            return Page();
        }
        public IActionResult OnGetCheckLogin(string username, string password)
        {
            SignInModel sModel = provider.ValidateUser(username, password);
            bool isValid = false;
            if (sModel != null)
            {
                SessionManager session = new SessionManager(HttpContext.Session);
                session.Set<SignInResult>(SessionKeys.user, sModel);
                TempData["UserName"] = sModel.Username;
                TempData["UserType"] = sModel.UserType;
                TempData["UserEmail"] = sModel.UserEmail;
                TempData["UserId"] = sModel.UserId;
                isValid = true;
            }
            return new JsonResult(isValid);
        }
    }
}
