using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using NLog;
using ProviderNPI_Lambda.Context;
using ProviderNPI_Lambda.DAL;
using ProviderNPI_Lambda.Models;
using ProviderNPI_Lambda;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using ProviderNPI_Lambda.Utility;
namespace ProviderNPI_Lambda.Pages
{
    public class GridFilterDataModel : PageModel
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger();
        InMemoryNPIContext _memoryrcmContext = null;
        ProviderNPIDAL npidal = null;
        ProviderNPIDataContext _RCMContext = null;
        public GridFilterDataModel(ProviderNPIDataContext RCMContext, IHttpContextAccessor httpContextAccessor, IMemoryCache memoryCache)
        {
            _memoryrcmContext = new InMemoryNPIContext(RCMContext, httpContextAccessor, memoryCache);
            npidal = new ProviderNPIDAL();
            _RCMContext = RCMContext;
        }

        public void OnGet()
        {
        }
        public IActionResult OnGetSearchData(DataSourceLoadOptions loadOptions)
        {
            
            _logger.Debug(Newtonsoft.Json.JsonConvert.SerializeObject(loadOptions));
            var dsLoader = DataSourceLoader.Load(_RCMContext.tblNPIData, loadOptions);
            
            //var script = ((IObjectContextAdapter)dsLoader).ObjectContext.CreateDatabaseScript();
            
            return new JsonResult(dsLoader);

        }
        public IActionResult OnGetSaveFilterData(int UserId, string Filter,string filter_name,string FilterId)
        {
            string sQuery = "";
            int iFilter = 0;
            if (!string.IsNullOrEmpty(FilterId))
            { 
                iFilter = Convert.ToInt32(FilterId); 
            }
            if (!string.IsNullOrEmpty(Filter))
            {
                sQuery = StoreHelper.CreateFilterQuery(Newtonsoft.Json.JsonConvert.DeserializeObject<System.Collections.IList>(Filter));
            }
            List<User_Filter_Rel> lUser_Filter_Rel = npidal.SaveFilters(UserId, Filter,filter_name, iFilter, sQuery);
            return new JsonResult(lUser_Filter_Rel);
        }
        public IActionResult OnGetShowFilterData(int UserId)
        {
            List<User_Filter_Rel> lUser_Filter_Rel= npidal.GetFilters(UserId);
            return new JsonResult(lUser_Filter_Rel);
        }
    }
}
