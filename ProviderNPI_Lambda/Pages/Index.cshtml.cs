using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Caching.Memory;
using PRM.SessionHandler;
using ProviderNPI_Lambda.Context;
using ProviderNPI_Lambda.DAL;
using ProviderNPI_Lambda.Models;

namespace ProviderNPI_Lambda.Pages
{
    public class IndexModel : PageModel
    {
        InMemoryNPIContext _memNPIContext = null;
        InMemoryStateContext _memStateContext = null;
        InMemoryCityContext _memCityContext = null;
        InMemoryTaxonomyContext _memTaxonomyContext = null;
        ProviderNPIDAL codeDAL = null;
        ProviderNPIDataContext _RCMContext = null;
        public IndexModel(ProviderNPIDataContext RCMContext, IHttpContextAccessor httpContextAccessor, IMemoryCache memoryCache)
        {
            _memNPIContext = new InMemoryNPIContext(RCMContext, httpContextAccessor, memoryCache);
            _memStateContext = new InMemoryStateContext(RCMContext, httpContextAccessor, memoryCache);
            _memCityContext = new InMemoryCityContext(RCMContext, httpContextAccessor, memoryCache);
            _memTaxonomyContext = new InMemoryTaxonomyContext(RCMContext, httpContextAccessor, memoryCache);
            codeDAL = new ProviderNPIDAL();
            _RCMContext = RCMContext;
            //codeDAL.TruncateTable();
        }
        
        public IActionResult OnGet()
        {
            var sModel = new SessionManager(HttpContext.Session).Get<SignInModel>(SessionKeys.user);
            if (sModel != null)
            {
                TempData["UserName"] = sModel.Username;
                TempData["UserType"] = sModel.UserType;
                TempData["UserEmail"] = sModel.UserEmail;
                TempData["UserId"] = sModel.UserId;
            }
            
            return Page();
        }
        string CreateWhereClause(string NPINumber, string PracticeName, string Taxonomy, string State, string City)
        {
            string sQuery = "";
            if (!string.IsNullOrEmpty(NPINumber)) { sQuery = sQuery + string.Format(" NPI={0} and", NPINumber); }
            if (!string.IsNullOrEmpty(PracticeName)) { sQuery = sQuery + string.Format(" and Provider_Organization_Name_Legal_Business_Name like '%{0}%'", PracticeName); }
            if (!string.IsNullOrEmpty(Taxonomy)) { sQuery = sQuery + string.Format(" and Healthcare_Provider_Taxonomy_Code_1 like '{0}'", Taxonomy); }
            if (!string.IsNullOrEmpty(State)) { sQuery = sQuery + string.Format(" and Provider_Business_Practice_Location_Address_State_Name like '{0}'", State); }
            if (!string.IsNullOrEmpty(City)) { sQuery = sQuery + string.Format(" and Provider_Business_Practice_Location_Address_City_Name like '{0}'", City); }
            if (sQuery.StartsWith(" and")) { sQuery = sQuery[4..]; }
            if (sQuery.EndsWith("and")) { sQuery = sQuery[0..^3]; }
            if (!string.IsNullOrEmpty(sQuery))
            {
                sQuery = " where " + sQuery;
            }
            return sQuery;
        }
        public IEnumerable<NPIData> GetNPIDataList(string NPI, string practiceName, string taxonomy, string state, string city)
        {
            IEnumerable<NPIData> lNPIData = new List<NPIData>();
            _memNPIContext.sWhere = CreateWhereClause(NPI, practiceName, taxonomy, state, city);
            if (!string.IsNullOrEmpty(NPI)) { 
                lNPIData = _memNPIContext.tblNPIData.ToList(); 
            }

            if (!string.IsNullOrEmpty(practiceName) && lNPIData.Count() == 0)
            {
                lNPIData = _memNPIContext.tblNPIData.ToList();
            }
            else if (!string.IsNullOrEmpty(practiceName))
            {
                lNPIData = lNPIData.Where(x => x.Provider_Organization_Name_Legal_Business_Name.Contains(practiceName)).ToList();
            }
            if (!string.IsNullOrEmpty(taxonomy) && lNPIData.Count() == 0)
            {
                lNPIData = _memNPIContext.tblNPIData;
            }
            else if (!string.IsNullOrEmpty(taxonomy))
            {
                lNPIData = lNPIData.Where(x => x.Healthcare_Provider_Taxonomy_Code_1.Contains(taxonomy)).ToList();
            }

            if (!string.IsNullOrEmpty(state) && lNPIData.Count() == 0) 
            { 
                lNPIData = _memNPIContext.tblNPIData.ToList();
            }
            else if (!string.IsNullOrEmpty(state))
            {
                lNPIData = lNPIData.Where(x => x.Provider_Business_Practice_Location_Address_State_Name.Contains(state)).ToList();
            }

            if (!string.IsNullOrEmpty(city) && lNPIData.Count() == 0)
            {
                lNPIData = _memNPIContext.tblNPIData.ToList();
            }
            else if (!string.IsNullOrEmpty(city))
            {
                lNPIData = lNPIData.Where(x => x.Provider_Business_Practice_Location_Address_City_Name.Contains(city)).ToList();
            }
            if (!string.IsNullOrEmpty(NPI) && lNPIData.Count() > 0)
            {
                lNPIData = lNPIData.Where(x => x.NPI.ToString().Contains(NPI)).ToList();
            }
            //if (!string.IsNullOrEmpty(NPI) && lNPIData.Count() > 0)
            //{
            //    lNPIData = lNPIData.Where(x => x.NPI.ToString().Contains(NPI)).ToList();
            //}
            return lNPIData;
        }
        public IActionResult OnGetTaxonomyData(DataSourceLoadOptions loadOptions,string NPI,string practiceName,string taxonomy,string state,string city)
        {
            //IEnumerable<NPIData> lNPIData = GetNPIDataList(NPI, practiceName, taxonomy, state, city);
            //List<TaxonomyData> lTaxonomyData = new List<TaxonomyData>();
            //lTaxonomyData = _memTaxonomyContext.tblTaxonomies.ToList();

            //if (lNPIData.Count() > 0)
            //{
            //    lTaxonomyData = (from taxnomy in lTaxonomyData
            //                     join npi in lNPIData on taxnomy.Code equals npi.Healthcare_Provider_Taxonomy_Code_1
            //                     select new TaxonomyData
            //                     {
            //                         Grouping = taxnomy.Code + " " + taxnomy.Grouping + " " + taxnomy.Classification + " " + taxnomy.Specialization,
            //                         Code = taxnomy.Code
            //                     }).ToList();
            //}
            //else
            //{
            //    lTaxonomyData = (from taxnomy in lTaxonomyData
            //                     select new TaxonomyData
            //                     {
            //                         Grouping = taxnomy.Code + " " + taxnomy.Grouping + " " + taxnomy.Classification + " " + taxnomy.Specialization,
            //                         Code = taxnomy.Code
            //                     }).ToList();
            //}
            
            return new JsonResult(DataSourceLoader.Load(_RCMContext.tblTaxonomy, loadOptions));
        }

        public IActionResult OnGetStateData(DataSourceLoadOptions loadOptions, string NPI, string practiceName, string taxonomy, string state, string city)
        {
            //IEnumerable<NPIData> lNPIData = GetNPIDataList(NPI, practiceName, taxonomy, state, city);
            //if (lNPIData.Count() > 0)
            //{
            //    var lState = lNPIData.Select(x => x.Provider_Business_Practice_Location_Address_State_Name).Distinct().ToList();
            //    lNPIData = (from npi in lState
            //                select new NPIData
            //                {
            //                    Provider_Business_Practice_Location_Address_State_Name = npi
            //                }).Distinct().ToList();
                
            //}
            //else
            //{
            //    lNPIData = _memStateContext.tblStates.Select(x=> new NPIData {Provider_Business_Practice_Location_Address_State_Name=x.State  }).ToList();
            //}
            return new JsonResult(DataSourceLoader.Load(_RCMContext.tblStates, loadOptions));
        }
        public IActionResult OnGetCityData(DataSourceLoadOptions loadOptions, string NPI, string practiceName, string taxonomy, string state, string city)
        {
            //IEnumerable<NPIData> lNPIData = GetNPIDataList(NPI, practiceName, taxonomy, state, city);
            //if (lNPIData.Count() > 0)
            //{
            //    var lCity = lNPIData.Select(x => x.Provider_Business_Practice_Location_Address_City_Name).Distinct().ToList();
            //    lNPIData = (from npi in lCity
            //                select new NPIData
            //                {
            //                    Provider_Business_Practice_Location_Address_City_Name = npi
            //                }).Distinct().ToList();
            //}
            //else
            //{
            //    lNPIData = _memCityContext.tblCities.Select(x => new NPIData { Provider_Business_Practice_Location_Address_City_Name = x.City }).Distinct().ToList();
            //}
            return new JsonResult(DataSourceLoader.Load(_RCMContext.tblCities, loadOptions));
        }
        public IActionResult OnGetFilterData(string NPINumber, string PracticeName, string Taxonomy, string State, string City)
        {
            SetTempData(NPINumber, PracticeName, Taxonomy, State, City);
            return Partial("GridFilterData");
        }
        public void SetTempData(string NPINumber, string PracticeName, string Taxonomy, string State, string City)
        {
            TempData["NPINumber"] = NPINumber == null ? "" : NPINumber;
            TempData["PracticeName"] = PracticeName == null ? "" : PracticeName;
            TempData["NPINumber"] = NPINumber == null ? "" : NPINumber;
            TempData["Taxonomy"] = Taxonomy == null ? "" : Taxonomy;
            TempData["State"] = State == null ? "" : State;
            TempData["City"] = City == null ? "" : City;
        }

    }
}
