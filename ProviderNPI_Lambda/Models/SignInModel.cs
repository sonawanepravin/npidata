﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProviderNPI_Lambda.Models
{
    public class SignInModel
    {
        public int? UserId { get; set; } = null;
        public string Username { get; set; } = "rcmadmin";
        public string Password { get; set; }
        public string UserType { get; set; } = "Admin";
        public string FirstName { get; set; } = "RCM";
        public string LastName { get; set; } = "NPI Dada";
        public string UserEmail { get; set; } = "rcm@gmail.com";
    }

}
