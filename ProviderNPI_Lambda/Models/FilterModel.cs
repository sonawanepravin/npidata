﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProviderNPI_Lambda.Models
{
    public class FilterModel
    {
        public static class FilterComparator
        {
            public static string Equal { get; set; } = " = ";
            public static string NotEquals { get; set; } = " != ";
            public static string Contains { get; set; } = " like ";
            public static string GreaterThan { get; set; } = " > " ;
            public static string GreaterThanOrEqual { get; set; } = " >= ";
            public static string LessThan { get; set; } = " < ";
            public static string LessThanOrEqual { get; set; } = " <= ";
        }

    }
    public class DataGridSortViewModel
    {
        public bool desc { get; set; }
        public string selector { get; set; }
    }
}
