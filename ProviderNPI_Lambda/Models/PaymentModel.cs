﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProviderNPI_Lambda.Models
{

    [Table("payments")]
    public class payments
    {
        [Key]
        public int Payment_id { get; set; }
        public string Entity { get; set; }
        public string Patient { get; set; }
        [Column("Effective Date")]
        public DateTime? Effective_Date { get; set; }
        public string Description { get; set; }
        [Column("Payment Type")]
        public string Payment_Type { get; set; }
        public string Procedure { get; set; }
        [Column("Rendering Provider")]
        public string Rendering_Provider { get; set; }
        public string Location { get; set; }
        public DateTime? DOS { get; set; }
        public string Payer { get; set; }
        [Column("Posting Date")]
        public DateTime? Posting_Date { get; set; }
        public double? Amount { get; set; }
        public double? Unapplied { get; set; }
        [Column("Co Insurance")]
        public double Co_Insurance { get; set; }
        [Column("Co Payment")]
        public double? Co_Payment { get; set; }
        public double? Deductible { get; set; }
        public double? Adjustment { get; set; }
        public double? Approved { get; set; }
        [Column("Batch Identifier")]
        public string Batch_Identifier { get; set; }
        [Column("Batch Number")]
        public string Batch_Number { get; set; }
        [Column("Check Number")]
        public string Check_Number { get; set; }
        [Column("Check Date")]
        public DateTime? Check_Date { get; set; }
        [Column("Check Amount")]
        public double? Check_Amount { get; set; }
        [Column("Payment Method")]
        public string Payment_Method { get; set; }
        [Column("Primary Payer")]
        public string Primary_Payer { get; set; }
        [Column("Billed Amount")]
        public double? Billed_Amount { get; set; }
        [Column("Charge Balance")]
        public double? Charge_Balance { get; set; }
        [Column("Balance Responsibility")]
        public string Balance_Responsibility { get; set; }
        [Column("Entered By")]
        public string Entered_By { get; set; }
        [Column("Entered Date")]
        public DateTime? Entered_Date { get; set; }
        public double? Withhold { get; set; }
        public string Comments { get; set; }
        [Column("Charge Value")]
        public double? Charge_Value { get; set; }
        [Column("Tax ID")]
        public string Tax_ID { get; set; }
        [Column("Attending Provider")]
        public string Attending_Provider { get; set; }
        [Column("Modifier Code 1")]
        public string Modifier_Code_1 { get; set; }
        [Column("Modifier Code 2")]
        public string Modifier_Code_2 { get; set; }
        [Column("Modifier Code 3")]
        public string Modifier_Code_3 { get; set; }
        [Column("Modifier Code 4")]
        public string Modifier_Code_4 { get; set; }
        public double? Units { get; set; }
        [Column("Claim Number")]
        public string Claim_Number { get; set; }
        [Column("Payer Sender Number")]
        public string Payer_Sender_Number { get; set; }
        [Column("Primary Payer Sender Number")]
        public string Primary_Payer_Sender_Number { get; set; }
        [Column("Chart Number")]
        public string Chart_Number { get; set; }
        [Column("Patient Number")]
        public string Patient_Number { get; set; }
        [Column("Plan Name")]
        public string Plan_Name { get; set; }
        [Column("Supervising Physician")]
        public string Supervising_Physician { get; set; }
        [Column("Payer Priority")]
        public string Payer_Priority { get; set; }
        [Column("Credit ID")]
        public string Credit_ID { get; set; }
        [Column("Debit ID")]
        public string Debit_ID { get; set; }
        [Column("Financial Period")]
        public string Financial_Period { get; set; }
        [Column("Primary Policy Type")]
        public string Primary_Policy_Type { get; set; }
        [Column("Account Subtype")]
        public string Account_Subtype { get; set; }
        [Column("Merchant Service Transaction ID")]
        public string Merchant_Service_Transaction_ID { get; set; }

    }
    [DevExpress.Xpo.Persistent("vw_cptcode_pivot_grid")]
    public class VWCPTCodePivotGrid
    {
        [DevExpress.Xpo.Key]
        public int Payment_id { get; set; }
        public string CPTAndModifierCode { get; set; }
        public double Approved { get; set; }
    }


    //[DevExpress.Xpo.Persistent("vw_cptcode")]
    [Table("vw_cptcode")]
    public class VWCPTCode
    {
        //[DevExpress.Xpo.Key()]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Payment_Id { get; set; }
        public string CPTCode { get; set; }
        public string ModifierCode1 { get; set; }
        public string ModifierCode2 { get; set; }
        public string ModifierCode3 { get; set; }
        public string ModifierCode4 { get; set; }
        public double Min_Approved { get; set; }
        public double Mid_Approved { get; set; }
        public double Max_Approved { get; set; }
    }
    //[DevExpress.Xpo.Persistent("vw_cptcode_location")]
    [Table("vw_cptcode_location")]
    public class VWCPTCodeLocation
    {
        //[DevExpress.Xpo.Key()]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Payment_Id { get; set; }
        public string CPTCode { get; set; }
        public string ModifierCode1 { get; set; }
        public string ModifierCode2 { get; set; }
        public string ModifierCode3 { get; set; }
        public string ModifierCode4 { get; set; }
        public string Location { get; set; }
        public double Min_Approved { get; set; }
        public double Mid_Approved { get; set; }
        public double Max_Approved { get; set; }
    }
    //[DevExpress.Xpo.Persistent("vw_cptcode_location_provider")]
    [Table("vw_cptcode_location_provider")]
    public class VWCPTCodeLocationProvider
    {
        //[DevExpress.Xpo.Key()]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Payment_Id { get; set; }
        public string CPTCode { get; set; }
        public string ModifierCode1 { get; set; }
        public string ModifierCode2 { get; set; }
        public string ModifierCode3 { get; set; }
        public string ModifierCode4 { get; set; }
        public string Location { get; set; }
        public string Provider { get; set; }
        public double Min_Approved { get; set; }
        public double Mid_Approved { get; set; }
        public double Max_Approved { get; set; }

    }
    //[DevExpress.Xpo.Persistent("vw_cptcode_location_provider_payer")]
    [Table("vw_cptcode_location_provider_payer")]
    public class VWCPTCodeLocationProviderPayer
    {
        //[DevExpress.Xpo.Key()]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Payment_Id { get; set; }
        public string CPTCode { get; set; }
        public string ModifierCode1 { get; set; }
        public string ModifierCode2 { get; set; }
        public string ModifierCode3 { get; set; }
        public string ModifierCode4 { get; set; }
        public string Location { get; set; }
        public string Provider { get; set; }
        public string Payer { get; set; }
        public double Min_Approved { get; set; }
        public double Mid_Approved { get; set; }
        public double Max_Approved { get; set; }
    }
    //[DevExpress.Xpo.Persistent("vw_cptcode_location_provider_payer_plan")]
    [Table("vw_cptcode_location_provider_payer_plan")]
    public class VWCPTCodeLocationProviderPayerPlan
    {
        //[DevExpress.Xpo.Key()]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Payment_Id { get; set; }
        public string CPTCode { get; set; }
        public string ModifierCode1 { get; set; }
        public string ModifierCode2 { get; set; }
        public string ModifierCode3 { get; set; }
        public string ModifierCode4 { get; set; }
        public string Location { get; set; }
        public string Provider { get; set; }
        public string Payer { get; set; }
        public string PayerPlan { get; set; }
        public double Min_Approved { get; set; }
        public double Mid_Approved { get; set; }
        public double Max_Approved { get; set; }
    }

    public class User_Filter_Rel
    {
        public int User_Filter_Rel_Id { get; set; }
        public string Filters { get; set; }
        public string Filter_Name { get; set; }
    }
}
