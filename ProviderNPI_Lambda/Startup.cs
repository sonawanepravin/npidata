using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using NLog.Web;
using ProviderNPI_Lambda.Context;
using System.IO;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.Extensions.Logging;

namespace ProviderNPI_Lambda
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            AppSettings.AppSettingSetup(Configuration);
            services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));
            
            services.AddLogging().AddDbContext<ProviderNPIDataContext>(options => options
               .UseMySql(Environment.GetEnvironmentVariable("RCMContext"),
               x => x.CommandTimeout((int)TimeSpan.FromMinutes(10).TotalSeconds))
            .UseLoggerFactory(LoggerFactory.Create(builder => builder.AddConsole())));

            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            TimeSpan SessionTimeSpan = new TimeSpan(0, 0, 0);
            services
               .AddMemoryCache()
               .AddSession(s => {
                   s.Cookie.Name = "RCM Project";
                   // Set a short timeout for easy testing.
                   s.IdleTimeout = TimeSpan.FromHours(10);
                   // You might want to only set the application cookies over a secure connection:
                   s.Cookie.SecurePolicy = CookieSecurePolicy.Always;
                   s.Cookie.SameSite = SameSiteMode.Strict;
                   s.Cookie.HttpOnly = true;
                   // Make the session cookie essential
                   s.Cookie.IsEssential = true;
               });
            //services.AddDataProtection().PersistKeysToAWSSystemsManager("DataProtection");
            services.AddRazorPages().AddJsonOptions(options => options.JsonSerializerOptions.PropertyNamingPolicy = null);
            //services.AddScoped<ILoadPayments,LoadPayments>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();
            app.UseSession();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapRazorPages();
            });
        }
    }
}
