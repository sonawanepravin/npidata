﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace ProviderNPI_Lambda
{
    public static class AppSettings
    {
        
        internal static void AppSettingSetup(IConfiguration configuration)
        {
            EnvironmentVariableTarget target = EnvironmentVariableTarget.Process;

            string ConStr = configuration["RCMContext"];
            Environment.SetEnvironmentVariable("RCMContext", ConStr, target);
        }
        public static int SetPropertyValue<T>(this T @this, string propertyName, int value)
        {
            try
            {
                Type type = @this.GetType();
                type.GetProperty(propertyName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static).SetValue(@this, value);
            }
            catch (Exception)
            {

            }
            return value;
        }
        public static string ToTraceString<T>(this IQueryable<T> t)
        {
            string sql = "";
            ObjectQuery<T> oqt = t as ObjectQuery<T>;
            if (oqt != null)
                sql = oqt.ToTraceString();
            return sql;
        }
    }
}
