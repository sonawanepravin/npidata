﻿// Write your Javascript code.
function showloading() {
    $(".loader").attr("style", "z-index:999;");
    //$(".modal-backdrop.fade.show").remove();
    $("#mShowProgress").modal({
        backdrop: 'static',
        keyboard: true,
        show: true
    });
}

function hideloading() {
    $(".loader").attr("style", "display:none");
    $("#mShowProgress").modal("hide");

}
var Alertmessages = {
    "SaveSuccess": "Record Saved Successfully",
    "ErrorMessage": "Something went wrong.",
    "CheckData": "Please check the entered values",
    "FailedToSave": "Failed to Save the record.",
    "SelectTheRecordToEdit": "Select The Record To Edit",
    "PleaseEnterFilterName": "Please Enter Filter Name"
}

function ShowAlertErrorMessage(message) {
    window.$.alert({
        title: 'Alert!',
        content: message,
        type: 'red',
        icon: 'fa fa-warning',
    });
}

function ShowAlertSuccessMessage(message) {
    window.$.alert({
        title: 'Success!',
        content: message,
        type: 'green',
        icon: 'fa fa-check',
    });
}

function ShowAlertWarningMessage(message) {
    window.$.alert({
        title: 'Warning!',
        content: message,
        type: 'blue',
        icon: 'fa fa-exclamation-triangle',
    });
}

function geterrorcode(status) {

    switch (status) {
        case 0:
            return "requested url is not reachable";
        case 400:
            return "Bad Request";
            break;
        case 401:
            return "Unauthorized ";
            break;
        case 402:
            return "Payment Required";
            break;
        case 403:
            return "Forbidden";
            break;
        case 404:
            return "Page Not Found";
            break;
        case 405:
            return "Method Not Allowed ";
            break;
        case 406:
            return "Not Acceptable";
            break;
        case 407:
            return "Proxy Authentication Required";
            break;
        case 408:
            return "Request Timeout";
            break;
        case 409:
            return "Conflict";
            break;
        case 410:
            return "Gone";
            break;
        case 411:
            return "Length Required";
            break;
        case 412:
            return "Precondition Failed";
            break;
        case 413:
            return "Payload Too Large";
            break;
        case 414:
            return "URI Too Long";
            break;
        case 415:
            return "Unsupported Media Type";
            break;
        case 416:
            return "Range Not Satisfiable";
            break;
        case 417:
            return "Expectation Failed";
            break;
        case 418:
            return "I'm a teapot";
            break;
        case 419:
            return "Page Expired ";
            break;
        case 420:
            return "Method Failure";
            break;
        case 430:
            return "Request Header Fields Too Large";
            break;
        case 450:
            return "Blocked by Windows Parental Controls";
            break;
        case 498:
            return "Invalid Token";
            break;
        case 499:
            return "Token Required";
            break;
        case 509:
            return "Bandwidth Limit Exceeded";
            break;
        case 526:
            return "Invalid SSL Certificate";
            break;
        case 529:
            return " Site is overloaded";
            break;
        case 530:
            return "Site is frozen";
            break;
        case 598:
            return "Network read timeout error";
            break;
        case 422:
            return "Unprocessable Entity";
            break;
        case 423:
            return "Locked";
            break;
        case 424:
            return "Failed Dependency";
            break;
        case 425:
            return "Too Early";
            break;
        case 426:
            return "Upgrade Required";
            break;
        case 428:
            return "Precondition Required";
            break;
        case 429:
            return "Too Many Requests";
            break;
        case 440:
            return "Login Time-out";
            break;
        case 451:
            return "Unavailable For Legal Reasons";
            break;
        case 500:
            return "Internal Server Error";
            break;
        case 501:
            return "Not Implemented";
            break;
        case 502:
            return "Bad Gateway";
            break;
        case 503:
            return "Service Unavailable";
            break;
        case 504:
            return "Gateway timeout";
            break;
        case 505:
            return "HTTP Version Not Supported";
            break;
        case 506:
            return "Variant Also Negotiates";
            break;
        case 507:
            return "Insufficient Storage";
            break;
        case 508:
            return "Loop Detected";
            break;
        case 510:
            return "Not Extended";
            break;
        case 511:
            return "Network Authentication Required";
            break;
        case 103:
            return "Checkpoint";
            break;
        case 440:
            return "Login Time-out";
            break;
        case 449:
            return "Retry With";
            break;
        case 451:
            return "Redirect";
            break;
        case 444:
            return "No Response";
            break;
        case 494:
            return "Request header too large";
            break;
        case 495:
            return "SSL Certificate Error";
            break;
        case 496:
            return "SSL Certificate Required";
            break;
        case 497:
            return "HTTP Request Sent to HTTPS Port";
            break;
        case 499:
            return " Client Closed Request";
            break;
        case 520:
            return "Web Server Returned an Unknown Error";
            break;
        case 521:
            return "Web Server Is Down";
            break;
        case 522:
            return "Connection Timed Out";
            break;
        case 523:
            return "Origin Is Unreachable";
            break;
        case 524:
            return "A Timeout Occurred";
            break;
        case 525:
            return "SSL Handshake Failed";
            break;
        case 526:
            return "Invalid SSL Certificate";
            break;
        case 527:
            return "Railgun Error";
            break;
        default:
            return "Database Connection Failure";
            break;

    }
}

var FilterDataField = [
    {
        "dataField": "NPI", "dataType": "number", "caption": "NPI", "allowFiltering": false, "allowHeaderFiltering": true
    },
    { "dataField": "Entity_Type_Code", "dataType": "number", "caption": "Entity Type Code",  "allowFiltering": true, "allowHeaderFiltering": false },
    { "dataField": "Replacement_NPI", "dataType": "string", "caption": "Replacement NPI", "filterOperations": ["=", "<>", "isblank", "isnotblank"], "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Employer_Identification_Number_EIN", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"], "caption": "Employer Identification Number (EIN)", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Provider_Organization_Name_Legal_Business_Name", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Provider Organization Name (Legal Business Name)", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Provider_Last_Name_Legal_Name", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Provider Last Name (Legal Name)", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Provider_First_Name", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Provider First Name", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Provider_Middle_Name", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Provider Middle Name", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Provider_Name_Prefix_Text", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Provider Name Prefix Text", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Provider_Name_Suffix_Text", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Provider Name Suffix Text", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Provider_Credential_Text", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Provider Credential Text", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Provider_Other_Organization_Name", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Provider Other Organization Name", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Provider_Other_Organization_Name_Type_Code", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Provider Other Organization Name Type Code", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Provider_Other_Last_Name", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Provider Other Last Name", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Provider_Other_First_Name", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Provider Other First Name", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Provider_Other_Middle_Name", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Provider Other Middle Name", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Provider_Other_Name_Prefix_Text", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Provider Other Name Prefix Text", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Provider_Other_Name_Suffix_Text", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Provider Other Name Suffix Text", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Provider_Other_Credential_Text", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Provider Other Credential Text", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Provider_Other_Last_Name_Type_Code", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Provider Other Last Name Type Code", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Provider_First_Line_Business_Mailing_Address", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Provider First Line Business Mailing Address", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Provider_Second_Line_Business_Mailing_Address", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Provider Second Line Business Mailing Address", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Provider_Business_Mailing_Address_City_Name", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Provider Business Mailing Address City Name", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Provider_Business_Mailing_Address_State_Name", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Provider Business Mailing Address State Name", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Provider_Business_Mailing_Address_Postal_Code", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Provider Business Mailing Address Postal Code", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Provider_Business_Mailing_Address_Country_Code_out_US", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Provider Business Mailing Address Country Code (If outside U.S.)", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Provider_Business_Mailing_Address_Telephone_Number", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Provider Business Mailing Address Telephone Number", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Provider_Business_Mailing_Address_Fax_Number", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Provider Business Mailing Address Fax Number", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Provider_First_Line_Business_Practice_Location_Address", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Provider First Line Business Practice Location Address", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Provider_Second_Line_Business_Practice_Location_Address", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Provider Second Line Business Practice Location Address", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Provider_Business_Practice_Location_Address_City_Name", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Provider Business Practice Location Address City Name", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Provider_Business_Practice_Location_Address_State_Name", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Provider Business Practice Location Address State Name", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Provider_Business_Practice_Location_Address_Postal_Code", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Provider Business Practice Location Address Postal Code", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Provider_Business_Practice_Location_Address_Country_Code_out_US", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Provider Business Practice Location Address Country Code (If outside U.S.)", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Provider_Business_Practice_Location_Address_Telephone_Number", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Provider Business Practice Location Address Telephone Number", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Provider_Business_Practice_Location_Address_Fax_Number", "dataType": "number", "caption": "Provider Business Practice Location Address Fax Number", "allowFiltering": true, "allowHeaderFiltering": false },
    { "dataField": "Provider_Enumeration_Date", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Provider Enumeration Date", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Last_Update_Date", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Last Update Date", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "NPI_Deactivation_Reason_Code", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "NPI Deactivation Reason Code", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "NPI_Deactivation_Date", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "NPI Deactivation Date", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "NPI_Reactivation_Date", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "NPI Reactivation Date", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Provider_Gender_Code", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Provider Gender Code", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Authorized_Official_Last_Name", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Authorized Official Last Name", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Authorized_Official_First_Name", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Authorized Official First Name", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Authorized_Official_Middle_Name", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Authorized Official Middle Name", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Authorized_Official_Title_or_Position", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Authorized Official Title or Position", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Authorized_Official_Telephone_Number", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Authorized Official Telephone Number", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Healthcare_Provider_Taxonomy_Code_1", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Healthcare Provider Taxonomy Code", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Provider_License_Number_1", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Provider License Number", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Provider_License_Number_State_Code_1", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Provider License Number State Code", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Healthcare_Provider_Primary_Taxonomy_Switch_1", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Healthcare Provider Primary Taxonomy Switch", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Is_Sole_Proprietor", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Is Sole Proprietor", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Is_Organization_Subpart", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Is Organization Subpart", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Parent_Organization_LBN", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Parent Organization LBN", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Parent_Organization_TIN", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Parent Organization TIN", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Authorized_Official_Name_Prefix_Text", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Authorized Official Name Prefix Text", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Authorized_Official_Name_Suffix_Text", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Authorized Official Name Suffix Text", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Authorized_Official_Credential_Text", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Authorized Official Credential Text", "allowFiltering": false, "allowHeaderFiltering": true },
    { "dataField": "Healthcare_Provider_Taxonomy_Group_1", "dataType": "string", "filterOperations": ["=", "<>", "isblank", "isnotblank"],"caption": "Healthcare Provider Taxonomy Group", "allowFiltering": false, "allowHeaderFiltering": true }
];

