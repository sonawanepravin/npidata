﻿
debugger;
$(document).ready(function () {
     if (!$("#ShowFilteredDropDown").is(":hidden")) {
                        $("#dCreateFilter").hide();
                    }
    if (SelectedFilterData == null) {
        var createfilter = [];
        //if ($("#ShowFilteredDropDown").is(":hidden")) {
            var searchdata = {
                NPINumber: $("#txtNPINumber").val(),
                PracticeName: $("#txtPracticeName").val(),
                Taxonomy: "",
                State: "",
                City: "",
                Zip: $("#txtZip").val(),
            };

            if ($("#cmdTaxonomy").dxSelectBox('instance').option('value') != null && $("#cmdTaxonomy").dxSelectBox('instance').option('value') != "Code") {
                searchdata.Taxonomy =
                    $("#cmdTaxonomy").dxSelectBox('instance').option('value').Code;
            }
            if ($("#cmdState").dxSelectBox('instance').option('value') != null && $("#cmdState").dxSelectBox('instance').option('value') != "State") {
                searchdata.State =
                    $("#cmdState").dxSelectBox('instance').option('value').State;
            }
            if ($("#cmdCity").dxSelectBox('instance').option('value') != null && $("#cmdCity").dxSelectBox('instance').option('value') != "City") {
                searchdata.City =
                    $("#cmdCity").dxSelectBox('instance').option('value').City;
            }
            if ((searchdata.NPINumber == undefined || searchdata.NPINumber == "") && (searchdata.PracticeName == undefined || searchdata.PracticeName == "") && (searchdata.Taxonomy == undefined || searchdata.Taxonomy == "") && (searchdata.State == undefined || searchdata.State == "") && (searchdata.City == undefined || searchdata.City == "")) {
                $.alert({
                    title: 'Error!',
                    content: "Please search from any of the options",
                    type: 'red',
                    icon: 'fa fa-times',
                });
                return;
            }

            if (searchdata.NPINumber != "" && searchdata.NPINumber != undefined) {
                createfilter.push(["NPI", "=", searchdata.NPINumber]);
            }
            if (searchdata.PracticeName != "" && searchdata.PracticeName != undefined) {
                if (createfilter != "") { createfilter.push("and") }
                createfilter.push(["Provider_Organization_Name_Legal_Business_Name", "=", searchdata.PracticeName]);
            }
            if (searchdata.Taxonomy != "" && searchdata.Taxonomy != undefined) {
                if (createfilter != "") { createfilter.push("and") }
                createfilter.push(["Healthcare_Provider_Taxonomy_Code_1", "=", searchdata.Taxonomy]);
            }
            if (searchdata.State != "" && searchdata.State != undefined) {
                if (createfilter != "") { createfilter.push("and") }
                createfilter.push(["Provider_Business_Practice_Location_Address_State_Name", "=", searchdata.State]);
            }
            if (searchdata.City != "" && searchdata.City != undefined) {
                if (createfilter != "") { createfilter.push("and") }
                createfilter.push(["Provider_Business_Practice_Location_Address_City_Name", "=", searchdata.City]);
            }
            if (searchdata.Zip != "" && searchdata.Zip != undefined) {
                if (createfilter != "") { createfilter.push("and") }
                createfilter.push(["Provider_Business_Practice_Location_Address_Postal_Code", "=", searchdata.Zip]);
            }
        //}
    }
    else {
        createfilter = SelectedFilterData;
    }
   
    $("#filterBuilder").dxFilterBuilder({
        fields: FilterDataField,
        value: createfilter
    });
    $("#FilterData").dxDataGrid(
        {
            "dataSource":
            {
                "store": DevExpress.data.AspNet.createStore({
                    "key": "NPI",
                    "loadUrl": URLGridFilterData
		        })
            },
            "showBorders": true,
            "showRowLines": true,
            "filterRow": { "visible": true },
            "headerFilter": { "visible": true, "allowSearch": true },
            "groupPanel": { "visible": true },
            "grouping": { "autoExpandAll": false },
            "height": "calc(100% - 82px)", "width": "100%",
            "columnAutoWidth": true,
            "scrolling": { "mode": "virtual", "rowRenderingMode": "virtual", "showScrollbar": "always" },
            "columnChooser": { "allowSearch": true, "enabled": true, "mode": "select" },
            "remoteOperations": true,
            "onContentReady": function (e) {
                CPTCode_OnContentReady(e);
            },
            "paging": { "pageSize": 200 },
            "pager": {
                "allowedPageSizes": [10, 25, 50, 100, 200],
                "showPageSizeSelector": true,
                "showInfo": false,
                "visible": false,
                "showNavigationButtons": false
            },
            "columns": FilterDataField,
            filterValue: createfilter,
        }
    );
    if ($("#UserId").val() == "") {
        $("#dFilterName").attr("style", "display:none");
        $("#btnSaveApply").attr("style", "display:none");
    }

});


function ShowFilterDetails() {
    if ($("#UserId").val() == "") {
        $.confirm({
            title: 'Login!',
            content: 'You have to login to save the Filters!',
            buttons: {
                filter: function () {
                    $("#modFilterBuilder").show();
                    $("#dFilterName").attr("style", "display:none");
                    $("#btnSaveApply").attr("style", "display:none");
                },
                login: function () {
                    window.location.href = URLLogin;
                }
            }
        });
    }
    else {
        $("#modFilterBuilder").show();
    }
}

function CPTCode_OnContentReady(e) {
    $("#iProcessIcon").removeClass("fa fa-refresh fa-spin button-spinner");
    $("#iProcessIcon").addClass("fa fa-search");
}