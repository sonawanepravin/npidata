﻿using ProviderNPI_Lambda.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using ProviderNPI_Lambda.Utility;
using System.Data.Common;
using DevExpress.Xpo;
using DevExpress.Xpo.DB;

namespace ProviderNPI_Lambda.DAL
{
    public class ProviderNPIDAL
    {
        public SignInModel ValidateUser(string UserName, string Password)
        {
            SignInModel signIn = null;
            using (IDbConnection con = MySqlConnectionProvider.CreateConnection(Environment.GetEnvironmentVariable("RCMContext")))
            {
                IDataAdapter DataAdapter = DbCommandExt.CreateDataAdapter(con);
                IDbCommand cmd = con.CreateDbCMD(CommandType.Text, string.Format("select * from tbluser where UserName='{0}' and Password='{1}'", UserName, Password));
                DataTable dtData = cmd.ExecuteDataTable();
                foreach (DataRow item in dtData.Rows)
                {
                    signIn = new SignInModel()
                    {
                        Username = item["UserName"].ConvertDBNullToString(),
                        FirstName = item["FirstName"].ConvertDBNullToString(),
                        LastName = item["LastName"].ConvertDBNullToString(),
                        UserEmail = item["UserEmail"].ConvertDBNullToString(),
                        UserType = item["UserType"].ConvertDBNullToString(),
                        UserId = item["UserId"].ConvertDBNullToInt(),
                    };
                }
            }

            return signIn;
        }
        public List<User_Filter_Rel> GetFilters(int UserId)
        {
            
            List<User_Filter_Rel> lUser_Filter_Rel = new List<User_Filter_Rel>();
            using (IDbConnection con = MySqlConnectionProvider.CreateConnection(Environment.GetEnvironmentVariable("RCMContext")))
            {
                IDataAdapter DataAdapter = DbCommandExt.CreateDataAdapter(con);
                IDbCommand cmd = con.CreateDbCMD(CommandType.Text, string.Format("select user_filter_rel_id,filter,filter_name from tbluser_filter_rel where UserId={0}", UserId));
                DataTable dtData = cmd.ExecuteDataTable();
                foreach (DataRow item in dtData.Rows)
                {
                    lUser_Filter_Rel.Add(new User_Filter_Rel()
                    {
                        User_Filter_Rel_Id = item["user_filter_rel_id"].ConvertDBNullToInt(),
                        Filters = item["filter"].ConvertDBNullToString(),
                        Filter_Name = item["filter_name"].ConvertDBNullToString()
                    });
                }
            }

            return lUser_Filter_Rel;
        }
        public List<User_Filter_Rel> SaveFilters(int UserId,string filter, string filter_name, int filter_id,string sWhereClause)
        {
            string sQuery = "";
            if (filter_id > 0)
            { 
                sQuery = string.Format("update tbluser_filter_rel set  filter='{0}',filter_name='{1}', WhereClause='{2}' where user_filter_rel_id={3};", filter, filter_name, sWhereClause.Replace("'", "''"), filter_id); 
            }
            else { 
                sQuery = string.Format("Insert into tbluser_filter_rel (userid,filter,filter_name,WhereClause) values ({0},'{1}','{2}','{3}');", UserId, filter, filter_name, sWhereClause.Replace("'","''")); 
            }
            
            IDbConnection con = MySqlConnectionProvider.CreateConnection(Environment.GetEnvironmentVariable("RCMContext"));
            IDbCommand cmd = con.CreateDbCMD(CommandType.Text, sQuery);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
            return GetFilters(UserId);
        }
        public void TruncateTable()
        {
            IDbConnection con = MySqlConnectionProvider.CreateConnection(Environment.GetEnvironmentVariable("RCMContext"));
            IDbCommand cmd = con.CreateDbCMD(CommandType.Text, "truncate table tbluser_filter_rel;INSERT INTO `tbluser_filter_rel` VALUES (1,1,'[\"Entity_Type_Code\",\">\",0]','Entity_Type_Greater_0'),(2,1,'[[\"Entity_Type_Code\",\">\",0],\"and\",[\"Provider_Business_Practice_Location_Address_Fax_Number\",\"<>\",null]]','Entity_Type_Greater_0_Provider_Fax_Greater_0');");
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        string CreateWhereClause(string NPINumber, string PracticeName, string Taxonomy, string State, string City)
        {
            string sQuery = "";
            if (!string.IsNullOrEmpty(NPINumber)) { sQuery = sQuery + string.Format(" NPI={0} and", NPINumber); }
            if (!string.IsNullOrEmpty(PracticeName)) { sQuery = sQuery + string.Format(" and Provider_Organization_Name_Legal_Business_Name like '%{0}%'", PracticeName); }
            if (!string.IsNullOrEmpty(Taxonomy)) { sQuery = sQuery + string.Format(" and Healthcare_Provider_Taxonomy_Code_1 like '%{0}%'", Taxonomy); }
            if (!string.IsNullOrEmpty(State)) { sQuery = sQuery + string.Format(" and Provider_Business_Practice_Location_Address_State_Name like '%{0}%'", State); }
            if (!string.IsNullOrEmpty(City)) { sQuery = sQuery + string.Format(" and Provider_Business_Practice_Location_Address_City_Name like '%{0}%'", City); }
            if (sQuery.StartsWith(" and")) { sQuery = sQuery[4..]; }
            if (sQuery.EndsWith("and")) { sQuery = sQuery[0..^3]; }
            if (!string.IsNullOrEmpty(sQuery))
            {
                sQuery = " where " + sQuery;
            }
            return sQuery;
        }
        public List<TaxonomyData> GetTaxonomyData(string NPINumber, string PracticeName, string Taxonomy, string State, string City)
        {
            string sQuery = "";
            sQuery=CreateWhereClause(NPINumber, PracticeName, Taxonomy, State, City);
            //_memoryrcmContext.sWhere = " Where " + sQuery;
            DataTable dtData = new DataTable();
            List<TaxonomyData> lTaxonomyData = new List<TaxonomyData>();
            IDbConnection con = MySqlConnectionProvider.CreateConnection(Environment.GetEnvironmentVariable("RCMContext"));
            IDataAdapter DataAdapter = DbCommandExt.CreateDataAdapter(con);
            IDbCommand cmd = con.CreateDbCMD(CommandType.Text, string.Format("select concat_ws(' ',`Code`,`Grouping`,Classification,Specialization) as taxonomy ,`Code` from `npidata_search` npis inner join taxonomy tn on npis.Healthcare_Provider_Taxonomy_Code_1 = tn.code {0};", sQuery));
            dtData = cmd.ExecuteDataTable();
            foreach (DataRow item in dtData.Rows)
            {
                lTaxonomyData.Add(new TaxonomyData()
                {
                    Code = item["Code"].ConvertDBNullToString(),
                    Grouping= item["taxonomy"].ConvertDBNullToString()
                });
            }
            return lTaxonomyData;
        }
        //public List<StateData> GetStateData(string NPINumber, string PracticeName, string Taxonomy, string State, string City)
        //{
        //    string sQuery = "";
        //    sQuery = CreateWhereClause(NPINumber, PracticeName, Taxonomy, State, City);
        //    DataTable dtData = new DataTable();
        //    List<StateData> lStateData = new List<StateData>();
        //    IDbConnection con = MySqlConnectionProvider.CreateConnection(Environment.GetEnvironmentVariable("RCMContext"));
        //    IDataAdapter DataAdapter = DbCommandExt.CreateDataAdapter(con);
        //    IDbCommand cmd = con.CreateDbCMD(CommandType.Text, string.Format("select distinct Provider_Business_Mailing_Address_State_Name as State, func_inc_var_session(0)  AS `StateId` from `npidata_search` join(select func_inc_var_session(1)) as tbl {0} ", sQuery));
        //    dtData = cmd.ExecuteDataTable();
        //    foreach (DataRow item in dtData.Rows)
        //    {
        //        lStateData.Add(new StateData()
        //        {
        //           // State_Id = item["StateId"].ConvertDBNullToInt(),
        //            Provider_Business_Practice_Location_Address_State_Name = item["State"].ConvertDBNullToString()
        //        });
        //    }
        //    return lStateData;
        //}
        //public List<CityData> GetCityData(string NPINumber, string PracticeName, string Taxonomy, string State, string City)
        //{
        //    string sQuery = "";
        //    sQuery = CreateWhereClause(NPINumber, PracticeName, Taxonomy, State, City);
        //    DataTable dtData = new DataTable();
        //    List<CityData> lCityData = new List<CityData>();
        //    IDbConnection con = MySqlConnectionProvider.CreateConnection(Environment.GetEnvironmentVariable("RCMContext"));
        //    IDataAdapter DataAdapter = DbCommandExt.CreateDataAdapter(con);
        //    IDbCommand cmd = con.CreateDbCMD(CommandType.Text, string.Format("select  distinct Provider_Business_Mailing_Address_City_Name as `City`,func_inc_var_session(0)  AS `CityId` from `npidata_search` {0}; ", sQuery));
        //    dtData = cmd.ExecuteDataTable();
        //    foreach (DataRow item in dtData.Rows)
        //    {
        //        lCityData.Add(new CityData()
        //        {
        //            //City_Id = item["CityId"].ConvertDBNullToInt(),
        //            Provider_Business_Practice_Location_Address_City_Name = item["City"].ConvertDBNullToString()
        //        });
        //    }
        //    return lCityData;
        //}
    }
}
