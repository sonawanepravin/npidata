﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using System.Data.Common;
using System.Reflection;
using DevExpress.Xpo.DB;

namespace ProviderNPI_Lambda.DAL
{
    public static class DbCommandExt
    {
        public static IDataAdapter CreateDataAdapter(System.Data.IDbConnection connection, IDbCommand cmd = null) { return GetAdapter(MySqlConnectionProvider.CreateConnection(Environment.GetEnvironmentVariable("RCMContext")), cmd); }

        static IDataAdapter GetAdapter(IDbConnection connection, IDbCommand cmd = null)
        {
            var assembly = connection.GetType().Assembly;
            var @namespace = connection.GetType().Namespace;

            // Assumes the factory is in the same namespace
            var factoryType = assembly.GetTypes()
                                .Where(x => x.Namespace == @namespace)
                                .Where(x => x.IsSubclassOf(typeof(DbProviderFactory)))
                                .Single();

            // SqlClientFactory and OleDbFactory both have an Instance field.
            var instanceFieldInfo = factoryType.GetField("Instance", BindingFlags.Static | BindingFlags.Public);
            var factory = (DbProviderFactory)instanceFieldInfo.GetValue(null);

            DbDataAdapter adapter = factory.CreateDataAdapter();
            adapter.SelectCommand = (DbCommand)cmd;
            //command = cmd;
            return adapter;
        }
        public static IDbCommand CreateDbCMD(this IDbConnection con, CommandType cmdtype, string cmdtext)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = cmdtype;
            cmd.CommandText = cmdtext;
            cmd.Connection = con;
            return cmd;
        }

        public static IDbCommand CreateStoredProcedureDbCMD(this IDbConnection con, string cmdtext)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = cmdtext;
            cmd.Connection = con;
            return cmd;
        }

        public static void AddCMDParam(this IDbCommand cmd, string parametername, object value)
        {
            IDbDataParameter param = cmd.CreateParameter();
            param.ParameterName = parametername;
            if (value == null)
            {
                param.Value = DBNull.Value;
            }
            else
            { param.Value = value; }

            cmd.Parameters.Add(param);
        }

        public static void AddCMDParam(this IDbCommand cmd, string parametername, object value, DbType dbtype)
        {
            IDbDataParameter param = cmd.CreateParameter();
            param.ParameterName = parametername;
            param.Value = value == null ? DBNull.Value : value;
            param.DbType = dbtype;
            cmd.Parameters.Add(param);
        }

        public static IDbDataParameter AddCMDOutParam(this IDbCommand cmd, string parametername, DbType dbtype)
        {
            
            IDbDataParameter param = cmd.CreateParameter();
            param.ParameterName = parametername;
            param.DbType = dbtype;
            param.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(param);
            return param;
        }

        public static bool IsDBNull(this IDataReader dataReader, string columnName)
        {
            return dataReader[columnName] == DBNull.Value;
        }

        public static DataSet ExecuteDataSet(this IDbCommand cmd)
        {
            DataSet oDataSet = new DataSet();
            using (IDbConnection con = MySqlConnectionProvider.CreateConnection(Environment.GetEnvironmentVariable("RCMContext")))
            {
                cmd.CommandTimeout = 0;
                var oDataAdapter = CreateDataAdapter(con, cmd);
                oDataAdapter.Fill(oDataSet);
            }
            return oDataSet;
        }

        public static DataTable ExecuteDataTable(this IDbCommand cmd)
        {
            DataSet oDataTable = new DataSet();
            using (IDbConnection con = MySqlConnectionProvider.CreateConnection(Environment.GetEnvironmentVariable("RCMContext")))
            {
                cmd.CommandTimeout = 0;
                var oDataAdapter = CreateDataAdapter(con, cmd);

                oDataAdapter.Fill(oDataTable);
            }
            return oDataTable.Tables[0];
        }
    }

}
