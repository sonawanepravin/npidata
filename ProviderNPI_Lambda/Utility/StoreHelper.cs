﻿using ProviderNPI_Lambda.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace ProviderNPI_Lambda.Utility
{
    public static class StoreHelper
    {
        public class ParserParameters
        {
            public string Operators { get; set; }
            public string SearchType { get; set; } = "";
        }
        static int iCounter = 0; static string sSearchtype = "";
        public static string ParseFilters(string filterArray,string sQuery)
        {
            var sArray = Newtonsoft.Json.JsonConvert.DeserializeObject<object[]>(filterArray);
            
            foreach (var subquery in sArray)
            {
                if (subquery == null)
                {

                    sQuery = sQuery +" "+ "null";
                    continue;
                }
                if (subquery.IsValidJson(subquery))
                {
                    iCounter = 0;
                    sQuery=ParseFilters(subquery.ToString(),sQuery);
                    continue;
                }
                var pComparator = StoreHelper.ParseComparator(subquery.ToString());
                iCounter++;
                if (sQuery == "")
                {

                    sQuery = string.Format(" {0} ", pComparator.Operators);
                }
                else
                {
                    if (iCounter == 3)
                    {
                        string sDate = "";
                        try
                        {
                            DateTime isDate;
                            if (subquery.GetType().Name == "DateTime")
                            {
                                DateTime.TryParse(pComparator.Operators, out isDate);
                                sDate = isDate.ToString("yyyy-MM-dd");
                                pComparator.Operators = sDate;
                            }
                            
                        }
                        catch (Exception)
                        {}

                        iCounter = 0;
                        if (sArray[0].ToString().Contains("Confirmed"))
                        {
                            pComparator.Operators = pComparator.Operators.ToLower() == "no" ? "No" : "Yes";
                            sSearchtype = "";
                            sQuery = string.Format(" {0} '{2}{1}{2}' ", sQuery, pComparator.Operators, sSearchtype);
                            sQuery = sQuery.Replace("Confirmed   like", "Confirmed =");
                        }
                        //if (sQuery.ToLower().IndexOf("confirmed") > -1)
                        //{
                        //    pComparator.Operators = pComparator.Operators.ToLower() == "no" ? "No" : "Yes";
                        //    sSearchtype = "";
                        //    sQuery = string.Format(" {0} '{2}{1}{2}' ", sQuery, pComparator.Operators, sSearchtype);
                        //    sQuery = sQuery.Replace("Confirmed   like", "Confirmed =");
                        //}
                        else if (sQuery.ToLower().IndexOf("spanish") > -1)
                        {
                            pComparator.Operators = pComparator.Operators.ToLower() == "no" ? "No" : "Yes";
                            sSearchtype = "";
                            sQuery = string.Format(" {0} '{2}{1}{2}' ", sQuery, pComparator.Operators, sSearchtype);
                            sQuery = sQuery.Replace("spanish   like", "spanish =");
                        }
                        else if (subquery !=null && subquery.ToString().ToLower()=="and")
                        {
                            sSearchtype = "and ";
                            sQuery = string.Format(" {0} {1} ", sQuery, sSearchtype);
                            
                        }
                        else
                        { sQuery = string.Format(" {0} '{2}{1}{2}' ", sQuery, pComparator.Operators, sSearchtype);}
                        
                    }
                    else
                    {
                        sSearchtype = pComparator.SearchType;
                        if (pComparator.Operators == "<>" && sArray[2] == null)
                        { sQuery = sQuery + string.Format(" is not ", pComparator.Operators);  }
                        else if (sArray[1] !=null && sArray[1].ToString() == "=" && sArray[2] == null)
                        { 
                            sQuery = sQuery + string.Format(" (({0} is null) OR ({0} ='')) ", sArray[0]);
                            return sQuery;
                        }
                        else { sQuery = sQuery + string.Format(" {0} ", pComparator.Operators); }
                    }

                }
            }
            return sQuery;
        }

        public static ParserParameters ParseComparator(string pComparator)
        {
            ParserParameters pParameters = new ParserParameters();
            pParameters.Operators = pComparator;
            if (pComparator == "=")
                pParameters.Operators = FilterModel.FilterComparator.Equal;
            else if (pComparator == "contains")
            {
                pParameters.Operators = FilterModel.FilterComparator.Contains;
                pParameters.SearchType = "%";
            }
            else if (pComparator == ">")
                pParameters.Operators = FilterModel.FilterComparator.GreaterThan;
            else if (pComparator == ">=")
                pParameters.Operators = FilterModel.FilterComparator.GreaterThanOrEqual;
            else if (pComparator == "<")
                pParameters.Operators = FilterModel.FilterComparator.LessThan;
            else if (pComparator == "<=")
                pParameters.Operators = FilterModel.FilterComparator.LessThanOrEqual;
            else if (pComparator == "!=")
                pParameters.Operators = FilterModel.FilterComparator.NotEquals;
            else
            { return pParameters; }

            return pParameters;
        }
        public static string CreateFilterQuery(IList filters)
        {
            string sSearchtype = "";
            int iCounter = 0;
            string sQuery = "";
            foreach (var item in filters)
            {
                if (item.IsValidJson(item))
                {
                    if (sQuery.Contains(" ! ") || item.ToString().Contains("!"))
                    {
                        sQuery = StoreHelper.ParseFilters(item.ToString(), sQuery);
                        sQuery = sQuery.Replace("!", "!(");
                        sQuery = sQuery + ")";
                    }
                    else
                    {
                        sQuery = StoreHelper.ParseFilters(item.ToString(), sQuery);
                    }
                }
                else
                {
                    if (item.ToString() == "and") { sQuery = sQuery + " and "; continue; }
                    iCounter++;

                    var pComparator = StoreHelper.ParseComparator(item.ToString());
                    if (sQuery == "")
                    {
                        sQuery = string.Format(" {0} ", pComparator.Operators);
                    }
                    else
                    {
                        if (iCounter == 3)
                        {
                            string sDate = "";
                            try
                            {
                                DateTime isDate;
                                if (item.GetType().Name == "DateTime")
                                {
                                    DateTime.TryParse(pComparator.Operators, out isDate);
                                    sDate = isDate.ToString("yyyy-MM-dd");
                                    pComparator.Operators = sDate;
                                }
                            }
                            catch (Exception)
                            { }

                            iCounter = 0;
                            if (sQuery.ToLower().IndexOf("confirmed") > -1)
                            {
                                pComparator.Operators = pComparator.Operators.ToLower() == "no" ? "No" : "Yes";
                                sSearchtype = "";
                                sQuery = string.Format(" {0} '{2}{1}{2}' ", sQuery, pComparator.Operators, sSearchtype);
                                sQuery = sQuery.Replace("Confirmed   like", "Confirmed =");
                            }
                            else if (sQuery.ToLower().IndexOf("spanish") > -1)
                            {
                                pComparator.Operators = pComparator.Operators.ToLower() == "no" ? "No" : "Yes";
                                sSearchtype = "";
                                sQuery = string.Format(" {0} '{2}{1}{2}' ", sQuery, pComparator.Operators, sSearchtype);
                                sQuery = sQuery.Replace("spanish   like", "spanish =");
                            }
                            else
                            {
                                //    if (pComparator.Operators == "Processed" || pComparator.Operators == "Processing")
                                //    {
                                //        pComparator.Operators = pComparator.Operators == "Processed" ? "1" : "0";
                                //        sQuery = string.Format(" {0} '{2}{1}{2}' ", sQuery, pComparator.Operators.Replace('"', ' '), sSearchtype);
                                //    }
                                //    else
                                //    {
                                sQuery = string.Format(" {0} '{2}{1}{2}' ", sQuery, pComparator.Operators, sSearchtype);
                            }
                            //}
                        }
                        else
                        {
                            sSearchtype = pComparator.SearchType;
                            sQuery = sQuery + string.Format(" {0} ", pComparator.Operators, pComparator.SearchType);
                        }

                    }
                }
            }
            return sQuery;
        }

       public static string CreateFilterQuery(IList filters, Boolean value)
       {
            string sSearchtype = "";
            int iCounter = 0;
            string sQuery = "";
            foreach (var item in filters)
            {
                if (item.IsValidJson(item))
                {
                    if (sQuery.Contains(" ! ") || item.ToString().Contains("!"))
                    {
                        sQuery = StoreHelper.ParseFilters(item.ToString(), sQuery);
                        sQuery = sQuery.Replace("!", "!(");
                        sQuery = sQuery + ")";
                    }
                    else
                    {
                        sQuery = StoreHelper.ParseFilters(item.ToString(), sQuery);
                    }
                }
                else
                {
                    if (item.ToString() == "and") { sQuery = sQuery + " and "; continue; }
                    iCounter++;

                    var pComparator =  StoreHelper.ParseComparator(item.ToString());
                    if (sQuery == "")
                    {
                        sQuery = string.Format(" {0} ", pComparator.Operators);
                    }
                    else
                    {
                        if (iCounter == 3)
                        {
                            string sDate = "";
                            try
                            {
                                DateTime isDate;
                                if (item.GetType().Name == "DateTime")
                                {
                                    DateTime.TryParse(pComparator.Operators, out isDate);
                                    sDate = isDate.ToString("yyyy-MM-dd");
                                    pComparator.Operators = sDate;
                                }

                            }
                            catch (Exception)
                            { }

                            iCounter = 0;
                            if (sQuery.ToLower().IndexOf("confirmed") > -1)
                            {
                                //pComparator.Operators = pComparator.Operators.ToLower() == "no" ? "false" : "true";
                                pComparator.Operators = pComparator.Operators.ToLower() == "no" ? "No" : "Yes";
                                sSearchtype = "";
                                sQuery = string.Format(" {0} '{2}{1}{2}' ", sQuery, pComparator.Operators, sSearchtype);
                                sQuery = sQuery.Replace("Confirmed   like", "Confirmed =");
                            }
                            else
                            { sQuery = string.Format(" {0} '{2}{1}{2}' ", sQuery, pComparator.Operators, sSearchtype); }
                        }
                        else
                        {
                            sSearchtype = pComparator.SearchType;
                            sQuery = sQuery + string.Format(" {0} ", pComparator.Operators, pComparator.SearchType);
                        }

                    }
                }
            }
            return sQuery;
        }

    }
    /// <summary>
    /// if the object this method is called on is not null, runs the given function and returns the value.
    /// if the object is null, returns default(TResult)
    /// </summary>

}
