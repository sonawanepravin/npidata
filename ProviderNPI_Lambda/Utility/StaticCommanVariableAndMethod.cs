﻿using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ProviderNPI_Lambda.Utility
{
    public static class StaticCommanVariableAndMethod
    {
        public static string errorText { get; set; }
        public static void LogError(string sMessage)
        {
            StreamWriter sw = new StreamWriter("ErrorWriter.txt", true, System.Text.Encoding.Default);
            sw.Write(sMessage);
            sw.Close();
        }
        
        public static int ConvertDBNullToInt(this object sValue)
        {
            if (((sValue != null && string.IsNullOrEmpty(sValue.ToString())) || (sValue != null && string.IsNullOrWhiteSpace(sValue.ToString()))) || sValue == DBNull.Value)
            { return 0; }
            return Convert.ToInt32(sValue);
        }
        public static long ConvertDBNullToLong(this object sValue)
        {
            if (sValue == DBNull.Value)
            { return 0; }
            return Convert.ToInt64(sValue);
        }
        public static string ConvertJSONNullToString(this object sValue)
        {
            if (sValue == null)
            { return ""; }
            return sValue.ToString();
        }
        internal static double ConvertDBNullToDouble(this object sValue)
        {
            if (sValue == DBNull.Value)
            { return 0; }
            return Convert.ToDouble(sValue);
        }

        internal static string ConvertDBNullToString(this object sValue)
        {
            if (sValue == DBNull.Value)
            { return ""; }
            return Convert.ToString(sValue);
        }

        public static bool ConvertDBNullToBool(this object sValue)
        {
            if (sValue == DBNull.Value)
            { return false; }
            return Convert.ToBoolean(sValue);
        }
        public static DateTime? ConvertDBNullToDate(this object sValue)
        {
            if ((string)sValue == "" || sValue == DBNull.Value || sValue == null)
            { return null; }

            return Convert.ToDateTime(sValue, System.Globalization.CultureInfo.InvariantCulture);
        }
        public static object ConvertDateTimeToDBNull(this object sValue)
        {
            if (sValue == "" || sValue == DBNull.Value || sValue == null)
            { return DBNull.Value; }
            return sValue;
        }
    }
}
