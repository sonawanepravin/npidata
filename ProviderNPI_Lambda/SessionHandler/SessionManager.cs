﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PRM.SessionHandler
{
    public class SessionManager
    {
        ISession iSession;
        public SessionManager(ISession session)
        {
            iSession = session;
        }
        public void ClearSession()
        {
            iSession.Clear();
        }
        public void Set<T>(SessionKeys key, object value)
        {
            iSession.SetString(key.ToString(), JsonConvert.SerializeObject(value, Formatting.None));
        }
        public void Set(string key, string value)
        {
            iSession.SetString(key, value);
        }
        public T Get<T>(SessionKeys key)
        {
            string value = iSession.GetString(key.ToString());
            return value == null ? default(T) : JsonConvert.DeserializeObject<T>(value);
        }
    }
    public enum SessionKeys
    {
        user
    }
}
